// example.js
const { test, expect } = require('@playwright/test');
const csv = require('csv-parser');
const robot = require('robotjs');
// const filePath = './users.csv';
const { chromium } = require('playwright-extra')
const { firefox } = require('playwright');
const pluginStealth = require("puppeteer-extra-plugin-stealth");
const puppeteer = require('puppeteer-extra');
const pathToExtension = require('path').join(__dirname, 'noCaptchaAi-chrome-v1.3');
const schedule = require('node-schedule');
const AWS = require('aws-sdk');
const fs = require('fs');
const axios = require('axios');
// Configure AWS SDK with your credentials
AWS.config.update({
    accessKeyId: 'AKIA6GBMEKXMG24GXNN6',
    secretAccessKey: 'E7pD04+pKXaK8/C+WAo3gyz6EYPiGwpqiqMpCfmf',
    region: 'us-east-1'
});

// Create an S3 instance
const s3 = new AWS.S3();

// Specify the bucket name and the file path
const bucketName = 'ultranet.monetization.image.logs';


// Specify the key (file name) under which you want to save the file in the S3 bucket


// const CaptchaSolver = require('captcha-solver')

// const solver = new CaptchaSolver('browser')
// const codes = await solver.solve()

puppeteer.use(pluginStealth);

// Use RecaptchaPlugin



function getRandomInRange(min, max) {
    return Math.random() * (max - min) + min;
}

// Function to generate a random latitude
function getRandomLatitude() {
    return getRandomInRange(-90, 90);
}

// Function to generate a random longitude
function getRandomLongitude() {
    return getRandomInRange(-180, 180);
}
function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function runPlaywright(paramArray) {
    const startDateStr = paramArray.selectedDateRangeForScheduling[0];
    const endDateStr = paramArray.selectedDateRangeForScheduling[1];
    const maxRandomWait = Number(paramArray.maxRandomWait);
    const startDate = new Date(startDateStr);
    const endDate = new Date(endDateStr);
    console.log(startDate, endDate)
    let counter = 0;
    let totalRequest = 0;
    let totalRequestSent = 0;
    let totalRequestFailed = 0;
    let imageLink = ''
    let filePath;
    let job
    // Read the file from the local file system
    let fileContent;
    job = schedule.scheduleJob(startDate, async () => {
        let jobCounter = 0;
        const executionsPerDay = paramArray.executionsPerDay
        console.log("job loop", paramArray.executionsPerDay)
        if (jobCounter < executionsPerDay) {
            jobCounter++;
            randomEmail = Math.random().toString(36).substring(7);

            let browser
            let page
            for (const user of paramArray?.testsCreatedObject) {
                if (new Date() <= endDate) {
                    console.log("user and counter", paramArray?.testsCreatedObject.length, counter, user.simData.msisdn)
                    if (user.servicesObject === 1 && user.simData.services.facebook === true) {
                        try {
                            browser = await chromium.launch(
                                {
                                    headless: false,
                                    // proxy: {
                                    //     server: 'http://geo.iproyal.com:12321',
                                    //     username: 'iSk3szVWheMj4F2K',
                                    //     password: 'mlvmJrDV6mK3d4fH',
                                    // },
                                }
                            );
                            console.log(1)
                            page = await browser.newPage();
                            await page.goto('https://www.facebook.com/');
                            // await page.pause()
                            await page.type('//input[@id="email" and @name="email" and @data-testid="royal_email"]', "+" + user.simData.dialingCode + user.simData.msisdn);
                            await page.type('//input[@data-testid="royal_pass"]', user.simData.password);
                            await page.click('//button[@data-testid="royal_login_button"]')
                            await page.waitForTimeout(15000);
                            console.log(await page.locator('//button[@id="alt-PHONE-OTP"]').isVisible())
                            if (await page.locator('//button[contains(text(), "Resend code")]').isVisible()) {
                                totalRequestSent++;
                                console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                                await page.waitForTimeout(15000);
                                const key = `${user.jobObject.id}/${user.id}/image.png`;
                                filePath = './screenshot.png';
                                fileContent = fs.readFileSync(filePath);
                                // Set up the parameters for the S3 upload
                                const params = {
                                    Bucket: bucketName,
                                    Key: key,
                                    Body: fileContent
                                };

                                // Upload the file to S3
                                s3.upload(params, (err, data) => {
                                    if (err) {
                                        console.error('Error uploading file to S3:', err);
                                    } else {
                                        // Update the ACL after successful upload
                                        const aclParams = {
                                            Bucket: bucketName,
                                            Key: key,
                                            ACL: 'public-read' // Set the access control level to make the file public
                                        };

                                        s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                            if (aclErr) {
                                                console.error('Error updating ACL:', aclErr);
                                            } else {
                                                console.log('File uploaded successfully. Public URL:', data);
                                                const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                                const dataTest = {
                                                    imageLink: data.Location,
                                                    status: 'Success'
                                                };
                                                axios
                                                    .put(updateitems, dataTest).then((response) => {
                                                        console.log("Response is wow", response.data);
                                                    })
                                                    .catch((error) => {
                                                        console.log("Response is nah", error);
                                                    })
                                            }
                                        });
                                    }
                                });

                            }
                            else {
                                console.log("check false", await page.locator('(//p[contains(text(), "The phone number you entered")])[2]').textContent())
                                throw new Error(await page.locator('(//p[contains(text(), "The phone number you entered")])[2]').textContent());
                            }
                            totalRequest++;
                            await page.close();
                            await browser.close();
                        }
                        catch (error) {
                            const errorMessage = error.message || 'Unknown error';
                            totalRequestFailed++;
                            console.log("error alertssss", error)
                            console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                            await page.waitForTimeout(15000);
                            const key = `${user.jobObject.id}/${user.id}/image.png`;
                            filePath = './screenshot.png';
                            fileContent = fs.readFileSync(filePath);
                            // Set up the parameters for the S3 upload
                            const params = {
                                Bucket: bucketName,
                                Key: key,
                                Body: fileContent
                            };

                            // Upload the file to S3
                            s3.upload(params, (err, data) => {
                                if (err) {
                                    console.error('Error uploading file to S3:', err);
                                } else {
                                    // Update the ACL after successful upload
                                    const aclParams = {
                                        Bucket: bucketName,
                                        Key: key,
                                        ACL: 'public-read' // Set the access control level to make the file public
                                    };

                                    s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                        if (aclErr) {
                                            console.error('Error updating ACL:', aclErr);
                                        } else {
                                            console.log('File uploaded successfully. Public URL:', data);
                                            const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                            const dataTest = {
                                                imageLink: data.Location,
                                                status: 'Failed',
                                                reason: errorMessage
                                            };
                                            axios
                                                .put(updateitems, dataTest).then((response) => {
                                                    console.log("Response is wow", response.data);
                                                })
                                                .catch((error) => {
                                                    console.log("Response is nah", error);
                                                })
                                        }
                                    });
                                }
                            });
                            await page.close();
                            await browser.close();
                        }
                    }
                    // if (user.servicesObject === 2 && user.simData.services.google === true) {
                    //     try {
                    //         browser = await chromium.launch(
                    //             {
                    //                 headless: false,
                    //                 // proxy: {
                    //                 //     server: 'http://geo.iproyal.com:12321',
                    //                 //     username: 'iSk3szVWheMj4F2K',
                    //                 //     password: 'mlvmJrDV6mK3d4fH',
                    //                 // },
                    //             }
                    //         );
                    //         console.log(2)
                    //         page = await browser.newPage();
                    //         await page.goto('https://accounts.google.com/');
                    //         await page.pause();
                    //         await page.type('//input[@type="email"]', user.simData.msisdn);
                    //         await page.locator('(//span[text()="Next"])[1]').click();
                    //         await page.type('//input[@type="password"]', user.simData.password);
                    //         await page.locator('(//span[text()="Next"])[1]').click();
                    //         await page.close();
                    //         await browser.close();
                    //     }
                    //     catch (error) {
                    //         console.log("error alert", error)
                    //         console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                    //         // await page.screenshot({ path: '../tests/screenshot.png' });
                    //         await page.close();
                    //         await browser.close();
                    //     }
                    // }
                    if (user.servicesObject === 3 && user.simData.services.tiktok === true) {
                        try {

                            browser = await chromium.launch(
                                {
                                    headless: false,
                                    // proxy: {
                                    //     server: 'http://geo.iproyal.com:12321',
                                    //     username: 'iSk3szVWheMj4F2K',
                                    //     password: 'mlvmJrDV6mK3d4fH',
                                    // },
                                }
                            );
                            console.log(3)
                            page = await browser.newPage();
                            await page.goto('https://www.tiktok.com/login');
                            // await page.pause()
                            await page.click('//div[contains(text(), "Use phone")]')
                            await page.click('//div[@aria-controls="phone-country-code-selector-wrapper"]')
                            await page.type('//input[@id="login-phone-search"]', user.simData.countryObject.name);
                            await page.click(`//li[contains(@id, "${user.simData.dialingCode}")]`)
                            await page.type('//input[@name="mobile"]', user.simData.msisdn);
                            await page.click('//button[@data-e2e="send-code-button"]')
                            await page.waitForTimeout(15000);
                            const options = {
                                method: 'POST',
                                url: 'https://flycaptcha.p.rapidapi.com/captcha/tiktok/slide',
                                headers: {
                                    'content-type': 'application/json',
                                    'X-RapidAPI-Key': '35fc3eea9fmsh46a5ddef4d9cb98p1f8dfbjsn23ca46247ec0',
                                    'X-RapidAPI-Host': 'flycaptcha.p.rapidapi.com'
                                },
                                data: {
                                    url1: 'https://p16-oec-captcha-sg.ibyteimg.com/tos-alisg-i-jt9k1oekl8-sg/d581668d9ec34ca9b6d752ac81d67a11~tplv-jt9k1oekl8-2.jpeg',
                                    url2: 'https://p16-oec-captcha-sg.ibyteimg.com/tos-alisg-i-jt9k1oekl8-sg/c90dd0166ad543bba6d74026d5ac5a13~tplv-jt9k1oekl8-1.png',
                                    proxy: ''
                                }
                            };

                            try {
                                const response = await axios.request(options);
                                console.log(response.data);
                            } catch (error) {
                                console.error(error);
                            }
                            console.log(await page.locator('//button[contains(text(), "Resend code")]').isVisible())
                            if (await page.locator('//button[contains(text(), "Resend code")]').isVisible()) {
                                totalRequestSent++;
                                console.log("checkinggSuccess", await page.screenshot({ path: 'screenshot.png' }))
                                await page.waitForTimeout(15000);
                                const key = `${user.jobObject.id}/${user.id}/image.png`;
                                filePath = './screenshot.png';
                                fileContent = fs.readFileSync(filePath);
                                // Set up the parameters for the S3 upload
                                const params = {
                                    Bucket: bucketName,
                                    Key: key,
                                    Body: fileContent
                                };

                                // Upload the file to S3
                                s3.upload(params, (err, data) => {
                                    if (err) {
                                        console.error('Error uploading file to S3:', err);
                                    } else {
                                        // Update the ACL after successful upload
                                        const aclParams = {
                                            Bucket: bucketName,
                                            Key: key,
                                            ACL: 'public-read' // Set the access control level to make the file public
                                        };

                                        s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                            if (aclErr) {
                                                console.error('Error updating ACL:', aclErr);
                                            } else {
                                                console.log('File uploaded successfully. Public URL:', data);
                                                const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                                const dataTest = {
                                                    imageLink: data.Location,
                                                    status: 'Success'
                                                };
                                                axios
                                                    .put(updateitems, dataTest).then((response) => {
                                                        console.log("Response is wow", response.data);
                                                    })
                                                    .catch((error) => {
                                                        console.log("Response is nah", error);
                                                    })
                                            }
                                        });
                                    }
                                });

                            }
                            else {
                                console.log("check false", await page.locator('//span[@role="status"]').textContent())
                                throw new Error(await page.locator('//span[@role="status"]').textContent());
                            }
                            totalRequest++;
                            await page.close();
                            await browser.close();
                        }
                        catch (error) {
                            const errorMessage = error.message || 'Unknown error';
                            totalRequestFailed++;
                            console.log("error alertssss", error)
                            console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                            await page.waitForTimeout(15000);
                            const key = `${user.jobObject.id}/${user.id}/image.png`;
                            filePath = './screenshot.png';
                            fileContent = fs.readFileSync(filePath);
                            // Set up the parameters for the S3 upload
                            const params = {
                                Bucket: bucketName,
                                Key: key,
                                Body: fileContent
                            };

                            // Upload the file to S3
                            s3.upload(params, (err, data) => {
                                if (err) {
                                    console.error('Error uploading file to S3:', err);
                                } else {
                                    // Update the ACL after successful upload
                                    const aclParams = {
                                        Bucket: bucketName,
                                        Key: key,
                                        ACL: 'public-read' // Set the access control level to make the file public
                                    };

                                    s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                        if (aclErr) {
                                            console.error('Error updating ACL:', aclErr);
                                        } else {
                                            console.log('File uploaded successfully. Public URL:', data);
                                            const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                            const dataTest = {
                                                imageLink: data.Location,
                                                status: 'Failed',
                                                reason: errorMessage
                                            };
                                            axios
                                                .put(updateitems, dataTest).then((response) => {
                                                    console.log("Response is wow", response.data);
                                                })
                                                .catch((error) => {
                                                    console.log("Response is nah", error);
                                                })
                                        }
                                    });
                                }
                            });
                            await page.close();
                            await browser.close();
                        }
                    }
                    if (user.servicesObject === 4 && user.simData.services.uber === true) {
                        try {

                            browser = await chromium.launch(
                                {
                                    headless: false,
                                    // proxy: {
                                    //     server: 'http://geo.iproyal.com:12321',
                                    //     username: 'iSk3szVWheMj4F2K',
                                    //     password: 'mlvmJrDV6mK3d4fH',
                                    // },
                                }
                            );
                            console.log(4)
                            page = await browser.newPage();
                            await page.goto('https://auth.uber.com/v2/?breeze_local_zone=phx3&next_url=https%3A%2F%2Fdrivers.uber.com%2F%3F_ga%3D2.153749304.1445137950.1708865659-2136894965.1708684517%26_gl%3D1%252Azxamip%252A_ga%252AMjEzNjg5NDk2NS4xNzA4Njg0NTE3%252A_ga_XTGQLY6KPT%252AMTcwODg2NTY1OC4yLjAuMTcwODg2NTY1OC4wLjAuMA..%26marketing_vistor_id%3D05400bfa-64b7-443c-ba12-40044dbc334e%26uclick_id%3D1dffbba8-309a-4f78-926c-4dbf7907031b&state=SK3qhQgS13OrvXEu2TqsuaeCce8HVuc2XT_T_vdl_KQ%3D');
                            // await page.pause()
                            await page.type('//input[@id="PHONE_NUMBER_or_EMAIL_ADDRESS"]', user.simData.msisdn);
                            await page.click(`//span[contains(@class, "bq br")]`);
                            await page.click(`//div[contains(text(), "${user.simData.countryObject.name}")]`)
                            await page.click('//div[contains(text(), "Continue")]')
                            await page.waitForTimeout(15000);
                            console.log(await page.locator('//button[@id="alt-PHONE-OTP"]').isVisible())
                            if (await page.locator('//button[contains(text(), "Resend code")]').isVisible()) {
                                totalRequestSent++;
                                console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                                await page.waitForTimeout(15000);
                                const key = `${user.jobObject.id}/${user.id}/image.png`;
                                filePath = './screenshot.png';
                                fileContent = fs.readFileSync(filePath);
                                // Set up the parameters for the S3 upload
                                const params = {
                                    Bucket: bucketName,
                                    Key: key,
                                    Body: fileContent
                                };

                                // Upload the file to S3
                                s3.upload(params, (err, data) => {
                                    if (err) {
                                        console.error('Error uploading file to S3:', err);
                                    } else {
                                        // Update the ACL after successful upload
                                        const aclParams = {
                                            Bucket: bucketName,
                                            Key: key,
                                            ACL: 'public-read' // Set the access control level to make the file public
                                        };

                                        s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                            if (aclErr) {
                                                console.error('Error updating ACL:', aclErr);
                                            } else {
                                                console.log('File uploaded successfully. Public URL:', data);
                                                const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                                const dataTest = {
                                                    imageLink: data.Location,
                                                    status: 'Success'
                                                };
                                                axios
                                                    .put(updateitems, dataTest).then((response) => {
                                                        console.log("Response is wow", response.data);
                                                    })
                                                    .catch((error) => {
                                                        console.log("Response is nah", error);
                                                    })
                                            }
                                        });
                                    }
                                });

                            }
                            else {
                                console.log("check false", await page.locator('(//p[contains(text(), "The phone number you entered")])[2]').textContent())
                                throw new Error(await page.locator('(//p[contains(text(), "The phone number you entered")])[2]').textContent());
                            }
                            totalRequest++;
                            await page.close();
                            await browser.close();
                        }
                        catch (error) {
                            const errorMessage = error.message || 'Unknown error';
                            totalRequestFailed++;
                            console.log("error alertssss", error)
                            console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                            await page.waitForTimeout(15000);
                            const key = `${user.jobObject.id}/${user.id}/image.png`;
                            filePath = './screenshot.png';
                            fileContent = fs.readFileSync(filePath);
                            // Set up the parameters for the S3 upload
                            const params = {
                                Bucket: bucketName,
                                Key: key,
                                Body: fileContent
                            };

                            // Upload the file to S3
                            s3.upload(params, (err, data) => {
                                if (err) {
                                    console.error('Error uploading file to S3:', err);
                                } else {
                                    // Update the ACL after successful upload
                                    const aclParams = {
                                        Bucket: bucketName,
                                        Key: key,
                                        ACL: 'public-read' // Set the access control level to make the file public
                                    };

                                    s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                        if (aclErr) {
                                            console.error('Error updating ACL:', aclErr);
                                        } else {
                                            console.log('File uploaded successfully. Public URL:', data);
                                            const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                            const dataTest = {
                                                imageLink: data.Location,
                                                status: 'Failed',
                                                reason: errorMessage
                                            };
                                            axios
                                                .put(updateitems, dataTest).then((response) => {
                                                    console.log("Response is wow", response.data);
                                                })
                                                .catch((error) => {
                                                    console.log("Response is nah", error);
                                                })
                                        }
                                    });
                                }
                            });
                            await page.close();
                            await browser.close();

                        }
                    }
                    if (user.servicesObject === 5 && user.simData.services.amazon === true) {
                        try {

                            browser = await chromium.launch(
                                {
                                    headless: false,
                                    // proxy: {
                                    //     server: 'http://geo.iproyal.com:12321',
                                    //     username: 'iSk3szVWheMj4F2K',
                                    //     password: 'mlvmJrDV6mK3d4fH',
                                    // },
                                }
                            );
                            console.log(5)
                            page = await browser.newPage();
                            await page.goto('https://www.amazon.com/ap/signin');
                            // await page.pause()
                            await page.type('//input[@id="ap_email"]', user.simData.msisdn);
                            await page.click(`//input[contains(@id, "continue")]`);
                            await page.type('//input[@id="ap_password"]', user.simData.password);
                            await page.click(`//input[contains(@id, "signInSubmit")]`);

                            // await page.click(`//div[contains(text(), "${user.simData.countryObject.name}")]`)
                            // await page.click('//div[contains(text(), "Continue")]')
                            await page.waitForTimeout(15000);
                            console.log(await page.locator('//button[@id="alt-PHONE-OTP"]').isVisible())
                            if (await page.locator('//button[contains(text(), "Resend code")]').isVisible()) {
                                totalRequestSent++;
                                console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                                await page.waitForTimeout(15000);
                                const key = `${user.jobObject.id}/${user.id}/image.png`;
                                filePath = './screenshot.png';
                                fileContent = fs.readFileSync(filePath);
                                // Set up the parameters for the S3 upload
                                const params = {
                                    Bucket: bucketName,
                                    Key: key,
                                    Body: fileContent
                                };

                                // Upload the file to S3
                                s3.upload(params, (err, data) => {
                                    if (err) {
                                        console.error('Error uploading file to S3:', err);
                                    } else {
                                        // Update the ACL after successful upload
                                        const aclParams = {
                                            Bucket: bucketName,
                                            Key: key,
                                            ACL: 'public-read' // Set the access control level to make the file public
                                        };

                                        s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                            if (aclErr) {
                                                console.error('Error updating ACL:', aclErr);
                                            } else {
                                                console.log('File uploaded successfully. Public URL:', data);
                                                const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                                const dataTest = {
                                                    imageLink: data.Location,
                                                    status: 'Success'
                                                };
                                                axios
                                                    .put(updateitems, dataTest).then((response) => {
                                                        console.log("Response is wow", response.data);
                                                    })
                                                    .catch((error) => {
                                                        console.log("Response is nah", error);
                                                    })
                                            }
                                        });
                                    }
                                });

                            }
                            else {
                                console.log("check false", await page.locator('(//p[contains(text(), "The phone number you entered")])[2]').textContent())
                                throw new Error(await page.locator('(//p[contains(text(), "The phone number you entered")])[2]').textContent());
                            }
                            totalRequest++;
                            await page.close();
                            await browser.close();
                        }
                        catch (error) {
                            const errorMessage = error.message || 'Unknown error';
                            totalRequestFailed++;
                            console.log("error alertssss", error)
                            console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                            await page.waitForTimeout(15000);
                            const key = `${user.jobObject.id}/${user.id}/image.png`;
                            filePath = './screenshot.png';
                            fileContent = fs.readFileSync(filePath);
                            // Set up the parameters for the S3 upload
                            const params = {
                                Bucket: bucketName,
                                Key: key,
                                Body: fileContent
                            };

                            // Upload the file to S3
                            s3.upload(params, (err, data) => {
                                if (err) {
                                    console.error('Error uploading file to S3:', err);
                                } else {
                                    // Update the ACL after successful upload
                                    const aclParams = {
                                        Bucket: bucketName,
                                        Key: key,
                                        ACL: 'public-read' // Set the access control level to make the file public
                                    };

                                    s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                        if (aclErr) {
                                            console.error('Error updating ACL:', aclErr);
                                        } else {
                                            console.log('File uploaded successfully. Public URL:', data);
                                            const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                            const dataTest = {
                                                imageLink: data.Location,
                                                status: 'Failed',
                                                reason: errorMessage
                                            };
                                            axios
                                                .put(updateitems, dataTest).then((response) => {
                                                    console.log("Response is wow", response.data);
                                                })
                                                .catch((error) => {
                                                    console.log("Response is nah", error);
                                                })
                                        }
                                    });
                                }
                            });
                            await page.close();
                            await browser.close();

                        }
                    }
                    if (user.servicesObject === 6 && user.simData.services.yandex === true) {
                        try {

                            browser = await chromium.launch(
                                {
                                    headless: false,
                                    // proxy: {
                                    //     server: 'http://geo.iproyal.com:12321',
                                    //     username: 'iSk3szVWheMj4F2K',
                                    //     password: 'mlvmJrDV6mK3d4fH',
                                    // },
                                }
                            );
                            console.log(6)
                            page = await browser.newPage();
                            await page.goto('https://passport.yandex.com/auth?retpath=https%3A%2F%2Fmail.yandex.com');
                            // await page.pause()
                            await page.click('//button[@data-type="phone"]');
                            const inputSelector = '//input[@id="passp-field-phone"]';

                            // Clear the input field
                            await page.$eval(inputSelector, input => input.value = '');

                            // Type the new value into the input field
                            await page.type(inputSelector, "+" + user.simData.dialingCode + user.simData.msisdn);

                            await page.click(`//button[contains(@id, "passp:sign-in")]`);

                            // await page.click(`//div[contains(text(), "${user.simData.countryObject.name}")]`)
                            // await page.click('//div[contains(text(), "Continue")]')
                            await page.waitForTimeout(3000);
                            console.log(await page.locator('//button[contains(@data-t, "button:default:retry-to-request-code")]').isVisible())
                            if (await page.locator('//button[contains(@data-t, "button:default:retry-to-request-code")]').isVisible()) {
                                totalRequestSent++;
                                console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                                await page.waitForTimeout(3000);
                                const key = `${user.jobObject.id}/${user.id}/image.png`;
                                filePath = './screenshot.png';
                                fileContent = fs.readFileSync(filePath);
                                // Set up the parameters for the S3 upload
                                const params = {
                                    Bucket: bucketName,
                                    Key: key,
                                    Body: fileContent
                                };

                                // Upload the file to S3
                                s3.upload(params, (err, data) => {
                                    if (err) {
                                        console.error('Error uploading file to S3:', err);
                                    } else {
                                        // Update the ACL after successful upload
                                        const aclParams = {
                                            Bucket: bucketName,
                                            Key: key,
                                            ACL: 'public-read' // Set the access control level to make the file public
                                        };

                                        s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                            if (aclErr) {
                                                console.error('Error updating ACL:', aclErr);
                                            } else {
                                                console.log('File uploaded successfully. Public URL:', data);
                                                const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                                const dataTest = {
                                                    imageLink: data.Location,
                                                    status: 'Success'
                                                };
                                                axios
                                                    .put(updateitems, dataTest).then((response) => {
                                                        console.log("Response is wow", response.data);
                                                    })
                                                    .catch((error) => {
                                                        console.log("Response is nah", error);
                                                    })
                                            }
                                        });
                                    }
                                });

                            }
                            else {
                                console.log("check false", await page.locator('(//div[contains(@id, "field:input-phone:hint")])').textContent())
                                throw new Error(await page.locator('(//div[contains(@id, "field:input-phone:hint")])').textContent());
                            }
                            totalRequest++;
                            await page.close();
                            await browser.close();
                        }
                        catch (error) {
                            const errorMessage = error.message || 'Unknown error';
                            totalRequestFailed++;
                            console.log("error alertssss", error)
                            console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                            await page.waitForTimeout(15000);
                            const key = `${user.jobObject.id}/${user.id}/image.png`;
                            filePath = './screenshot.png';
                            fileContent = fs.readFileSync(filePath);
                            // Set up the parameters for the S3 upload
                            const params = {
                                Bucket: bucketName,
                                Key: key,
                                Body: fileContent
                            };

                            // Upload the file to S3
                            s3.upload(params, (err, data) => {
                                if (err) {
                                    console.error('Error uploading file to S3:', err);
                                } else {
                                    // Update the ACL after successful upload
                                    const aclParams = {
                                        Bucket: bucketName,
                                        Key: key,
                                        ACL: 'public-read' // Set the access control level to make the file public
                                    };

                                    s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                        if (aclErr) {
                                            console.error('Error updating ACL:', aclErr);
                                        } else {
                                            console.log('File uploaded successfully. Public URL:', data);
                                            const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                            const dataTest = {
                                                imageLink: data.Location,
                                                status: 'Failed',
                                                reason: errorMessage
                                            };
                                            axios
                                                .put(updateitems, dataTest).then((response) => {
                                                    console.log("Response is wow", response.data);
                                                })
                                                .catch((error) => {
                                                    console.log("Response is nah", error);
                                                })
                                        }
                                    });
                                }
                            });
                            await page.close();
                            await browser.close();

                        }
                    }
                    if (user.servicesObject === 7 && user.simData.services.yango === true) {
                        try {

                            browser = await chromium.launch(
                                {
                                    headless: false,
                                    // proxy: {
                                    //     server: 'http://geo.iproyal.com:12321',
                                    //     username: 'iSk3szVWheMj4F2K',
                                    //     password: 'mlvmJrDV6mK3d4fH',
                                    // },
                                }
                            );
                            console.log(7)
                            page = await browser.newPage();
                            await page.goto('https://passport.yango.com/auth/reg?origin=plus&retpath=https%3A%2F%2Fplus.yango.com%2Fgetplus%3Flang%3Den&lang=en');
                            // await page.pause()
                            await page.type('//input[@id="passp-field-phone"]', "+" + user.simData.dialingCode + user.simData.msisdn);
                            await page.click('//input[@data-t="checkbox"]');
                            await page.click(`//button[contains(@type, "submit")]`);
                            await page.waitForTimeout(2000);
                            console.log(await page.locator('//button[contains(@data-t, "button:default:retry-to-request-code")]').isVisible())
                            if (await page.locator('//button[contains(@data-t, "button:default:retry-to-request-code")]').isVisible()) {
                                totalRequestSent++;
                                console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                                await page.waitForTimeout(2000);
                                const key = `${user.jobObject.id}/${user.id}/image.png`;
                                filePath = './screenshot.png';
                                fileContent = fs.readFileSync(filePath);
                                // Set up the parameters for the S3 upload
                                const params = {
                                    Bucket: bucketName,
                                    Key: key,
                                    Body: fileContent
                                };

                                // Upload the file to S3
                                s3.upload(params, (err, data) => {
                                    if (err) {
                                        console.error('Error uploading file to S3:', err);
                                    } else {
                                        // Update the ACL after successful upload
                                        const aclParams = {
                                            Bucket: bucketName,
                                            Key: key,
                                            ACL: 'public-read' // Set the access control level to make the file public
                                        };

                                        s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                            if (aclErr) {
                                                console.error('Error updating ACL:', aclErr);
                                            } else {
                                                console.log('File uploaded successfully. Public URL:', data);
                                                const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                                const dataTest = {
                                                    imageLink: data.Location,
                                                    status: 'Success'
                                                };
                                                axios
                                                    .put(updateitems, dataTest).then((response) => {
                                                        console.log("Response is wow", response.data);
                                                    })
                                                    .catch((error) => {
                                                        console.log("Response is nah", error);
                                                    })
                                            }
                                        });
                                    }
                                });

                            }
                            else {
                                console.log("check false", await page.locator('(//div[contains(@id, "field:input-phone:hint")])').textContent())
                                throw new Error(await page.locator('(//div[contains(@id, "field:input-phone:hint")])').textContent());
                            }
                            totalRequest++;
                            await page.close();
                            await browser.close();
                        }
                        catch (error) {
                            const errorMessage = error.message || 'Unknown error';
                            totalRequestFailed++;
                            console.log("error alertssss", error)
                            console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                            await page.waitForTimeout(15000);
                            const key = `${user.jobObject.id}/${user.id}/image.png`;
                            filePath = './screenshot.png';
                            fileContent = fs.readFileSync(filePath);
                            // Set up the parameters for the S3 upload
                            const params = {
                                Bucket: bucketName,
                                Key: key,
                                Body: fileContent
                            };

                            // Upload the file to S3
                            s3.upload(params, (err, data) => {
                                if (err) {
                                    console.error('Error uploading file to S3:', err);
                                } else {
                                    // Update the ACL after successful upload
                                    const aclParams = {
                                        Bucket: bucketName,
                                        Key: key,
                                        ACL: 'public-read' // Set the access control level to make the file public
                                    };

                                    s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                        if (aclErr) {
                                            console.error('Error updating ACL:', aclErr);
                                        } else {
                                            console.log('File uploaded successfully. Public URL:', data);
                                            const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                            const dataTest = {
                                                imageLink: data.Location,
                                                status: 'Failed',
                                                reason: errorMessage
                                            };
                                            axios
                                                .put(updateitems, dataTest).then((response) => {
                                                    console.log("Response is wow", response.data);
                                                })
                                                .catch((error) => {
                                                    console.log("Response is nah", error);
                                                })
                                        }
                                    });
                                }
                            });
                            await page.close();
                            await browser.close();

                        }
                    }
                    if (user.servicesObject === 8 && user.simData.services.adobe === true) {
                        try {
                            counter++;
                            console.log("adobe test", counter, user.id)
                            browser = await firefox.launch(
                                {
                                    headless: false,
                                    // proxy: {
                                    //     server: 'http://geo.iproyal.com:12321',
                                    //     username: 'iSk3szVWheMj4F2K',
                                    //     password: 'mlvmJrDV6mK3d4fH',
                                    // },
                                }
                            );
                            page = await browser.newPage();
                            await page.goto('https://www.adobe.com/', { waitUntil: 'domcontentloaded' });
                            // await page.pause()
                            await page.click('//button[@daa-ll="Sign In"]');
                            await page.waitForTimeout(2000);
                            await page.fill('//input[@id="EmailPage-EmailField"]', user.simData.user_name);
                            await page.waitForTimeout(2000);
                            await page.click(`//button[contains(@data-id, "EmailPage-ContinueButton")]`);
                            await page.waitForTimeout(2000);
                            const last4Digits = user.simData.msisdn.slice(-4);
                            await page.type('//input[contains(@data-id, "CodeInput-0")]', last4Digits[0]);
                            await page.type('//input[contains(@data-id, "CodeInput-1")]', last4Digits[1]);
                            await page.waitForTimeout(2000);
                            if (await page.locator('//p[contains(text(), "Your account has been locked due to multiple failed login attempts. Please try again later. ")]').isVisible()) {
                                throw new Error(await page.locator('//p[contains(text(), "Your account has been locked due to multiple failed login attempts. Please try again later. ")]').textContent());
                            }
                            if (await page.locator('//input[contains(@data-id, "CodeInput-0")]').isVisible()) {
                                totalRequestSent++;
                                // await page.reload();
                                await page.screenshot({ path: 'screenshot.png' });
                                await page.waitForTimeout(2000);
                                const key = `${user.jobObject.id}/${user.id}/image.png`;
                                filePath = './screenshot.png';
                                fileContent = fs.readFileSync(filePath);
                                // Set up the parameters for the S3 upload
                                const params = {
                                    Bucket: bucketName,
                                    Key: key,
                                    Body: fileContent
                                };

                                // Upload the file to S3
                                await s3.upload(params, async (err, data) => {
                                    if (err) {
                                        console.error('Error uploading file to S3:', err);
                                    } else {
                                        // Update the ACL after successful upload
                                        const aclParams = {
                                            Bucket: bucketName,
                                            Key: key,
                                            ACL: 'public-read' // Set the access control level to make the file public
                                        };

                                        await s3.putObjectAcl(aclParams, async (aclErr, aclData) => {
                                            if (aclErr) {
                                                console.error('Error updating ACL:', aclErr);
                                            } else {
                                                console.log('File uploaded successfully. Public URL:', data);
                                                const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                                const dataTest = {
                                                    imageLink: data.Location,
                                                    status: 'Success'
                                                };
                                                await axios
                                                    .put(updateitems, dataTest).then((response) => {

                                                    })
                                                    .catch((error) => {

                                                    })
                                            }
                                        });
                                    }
                                });

                            }
                            else {

                                throw new Error(await page.locator('(//label[contains(@data-id, "EmailPage-EmailField-Error")])').textContent());
                            }
                            totalRequest++;
                            await page.close();
                            await browser.close();
                        }
                        catch (error) {
                            const errorMessage = error.message || 'Unknown error';
                            totalRequestFailed++;

                            await page.screenshot({ path: 'screenshot.png' })
                            await page.waitForTimeout(15000);
                            const key = `${user.jobObject.id}/${user.id}/image.png`;
                            filePath = './screenshot.png';
                            fileContent = fs.readFileSync(filePath);
                            // Set up the parameters for the S3 upload
                            const params = {
                                Bucket: bucketName,
                                Key: key,
                                Body: fileContent
                            };

                            // Upload the file to S3
                            s3.upload(params, (err, data) => {
                                if (err) {
                                    console.error('Error uploading file to S3:', err);
                                } else {
                                    // Update the ACL after successful upload
                                    const aclParams = {
                                        Bucket: bucketName,
                                        Key: key,
                                        ACL: 'public-read' // Set the access control level to make the file public
                                    };

                                    s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                        if (aclErr) {
                                            console.error('Error updating ACL:', aclErr);
                                        } else {

                                            const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                            const dataTest = {
                                                imageLink: data.Location,
                                                status: 'Failed',
                                                reason: errorMessage
                                            };
                                            axios
                                                .put(updateitems, dataTest).then((response) => {

                                                })
                                                .catch((error) => {

                                                })
                                        }
                                    });
                                }
                            });
                            await page.close();
                            await browser.close();
                        }
                    }
                    if (user.servicesObject === 9 && user.simData.services.bumble === true) {
                        try {

                            browser = await firefox.launch(
                                {
                                    headless: false,
                                    // proxy: {
                                    //     server: 'http://geo.iproyal.com:12321',
                                    //     username: 'iSk3szVWheMj4F2K',
                                    //     password: 'mlvmJrDV6mK3d4fH',
                                    // },
                                }
                            );
                            console.log(9)
                            page = await browser.newPage();
                            await page.goto('https://bumble.com/get-started', { waitUntil: 'domcontentloaded' });
                            await page.pause()
                            await page.click('//span[text()="Use cell phone number"]');
                            await page.click('//input[@id="phone-country-code"]');
                            await page.type('//input[@id="phone-country-code"]', user.simData.dialingCode.toString());
                            await page.type('//input[@id="phone"]', user.simData.msisdn);
                            await page.click(`//button[contains(@type, "submit")]`);
                            await page.waitForTimeout(5000);
                            console.log(await page.locator('//h2[contains(text(), "Next, please enter the 6-digit code we just sent you")]').isVisible())
                            if (await page.locator('//h2[contains(text(), "Next, please enter the 6-digit code we just sent you")]').isVisible()) {
                                totalRequestSent++;
                                console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                                await page.waitForTimeout(2000);
                                const key = `${user.jobObject.id}/${user.id}/image.png`;
                                filePath = './screenshot.png';
                                fileContent = fs.readFileSync(filePath);
                                // Set up the parameters for the S3 upload
                                const params = {
                                    Bucket: bucketName,
                                    Key: key,
                                    Body: fileContent
                                };

                                // Upload the file to S3
                                s3.upload(params, (err, data) => {
                                    if (err) {
                                        console.error('Error uploading file to S3:', err);
                                    } else {
                                        // Update the ACL after successful upload
                                        const aclParams = {
                                            Bucket: bucketName,
                                            Key: key,
                                            ACL: 'public-read' // Set the access control level to make the file public
                                        };

                                        s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                            if (aclErr) {
                                                console.error('Error updating ACL:', aclErr);
                                            } else {
                                                console.log('File uploaded successfully. Public URL:', data);
                                                const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                                const dataTest = {
                                                    imageLink: data.Location,
                                                    status: 'Success'
                                                };
                                                axios
                                                    .put(updateitems, dataTest).then((response) => {
                                                        console.log("Response is wow", response.data);
                                                    })
                                                    .catch((error) => {
                                                        console.log("Response is nah", error);
                                                    })
                                            }
                                        });
                                    }
                                });

                            }
                            else {
                                console.log("check false", await page.locator('(//div[contains(@class, "p-3 text-color-error")])').textContent())
                                throw new Error(await page.locator('(//div[contains(@class, "p-3 text-color-error")])').textContent());
                            }
                            totalRequest++;
                            await page.close();
                            await browser.close();
                        }
                        catch (error) {
                            if (error instanceof SpecificErrorType) {
                                // Handle this specific error type
                            } else {
                                // Handle other errors

                                const errorMessage = error.message || 'Unknown error';
                                totalRequestFailed++;
                                console.log("error alertssss", error)
                                console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                                await page.waitForTimeout(15000);
                                const key = `${user.jobObject.id}/${user.id}/image.png`;
                                filePath = './screenshot.png';
                                fileContent = fs.readFileSync(filePath);
                                // Set up the parameters for the S3 upload
                                const params = {
                                    Bucket: bucketName,
                                    Key: key,
                                    Body: fileContent
                                };

                                // Upload the file to S3
                                s3.upload(params, (err, data) => {
                                    if (err) {
                                        console.error('Error uploading file to S3:', err);
                                    } else {
                                        // Update the ACL after successful upload
                                        const aclParams = {
                                            Bucket: bucketName,
                                            Key: key,
                                            ACL: 'public-read' // Set the access control level to make the file public
                                        };

                                        s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                            if (aclErr) {
                                                console.error('Error updating ACL:', aclErr);
                                            } else {
                                                console.log('File uploaded successfully. Public URL:', data);
                                                const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                                const dataTest = {
                                                    imageLink: data.Location,
                                                    status: 'Failed',
                                                    reason: errorMessage
                                                };
                                                axios
                                                    .put(updateitems, dataTest).then((response) => {
                                                        console.log("Response is wow", response.data);
                                                    })
                                                    .catch((error) => {
                                                        console.log("Response is nah", error);
                                                    })
                                            }
                                        });
                                    }
                                });
                            }
                            await page.close();
                            await browser.close();

                        }
                    }
                    if (user.servicesObject === 10 && user.simData.services.binance === true) {
                        try {

                            browser = await chromium.launch(
                                {
                                    headless: false,
                                    // proxy: {
                                    //     server: 'http://geo.iproyal.com:12321',
                                    //     username: 'iSk3szVWheMj4F2K',
                                    //     password: 'mlvmJrDV6mK3d4fH',
                                    // },
                                }
                            );
                            console.log(9)
                            page = await browser.newPage();
                            await page.goto('https://www.binance.com/en-IN', { waitUntil: 'domcontentloaded' });
                            // await page.pause()

                            // await page.click('//button[@id="onetrust-accept-btn-handler"]');
                            await page.click('//a[@id="toLoginPage"]');

                            await page.waitForTimeout(2000);
                            await page.click('//input[@id="username"]');
                            await page.type('//input[@id="username"]', user.simData.msisdn);
                            await page.waitForTimeout(1000);
                            await page.click(`(//input[contains(@class, "bn-textField-input")])[2]`);
                            await page.type('(//input[contains(@class, "bn-textField-input")])[4]', user.simData.countryObject.name);
                            await page.click(`(//div[contains(text(), "${user.simData.countryObject.name}")])`);
                            await page.click('//button[@id="click_login_submit_v2"]');
                            await page.waitForTimeout(3000);
                            await page.type('(//input[contains(@name, "password")])', user.simData.password);
                            await page.click('//button[@id="click_login_submit_v2"]');
                            await page.click('//button[text()="Yes"]');
                            await page.click('//div[text()="Email"]');
                            await page.click('//button[text()="Enable"]');
                            await page.click('//div[text()="Get Code"]');
                            await page.click('//div[text()="SMS"]');
                            console.log(await page.locator('//div[text()="Code Sent"]').isVisible())
                            if (await page.locator('//div[text()="Code Sent"]').isVisible()) {
                                totalRequestSent++;
                                console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                                await page.waitForTimeout(2000);
                                const key = `${user.jobObject.id}/${user.id}/image.png`;
                                filePath = './screenshot.png';
                                fileContent = fs.readFileSync(filePath);
                                // Set up the parameters for the S3 upload
                                const params = {
                                    Bucket: bucketName,
                                    Key: key,
                                    Body: fileContent
                                };

                                // Upload the file to S3
                                s3.upload(params, (err, data) => {
                                    if (err) {
                                        console.error('Error uploading file to S3:', err);
                                    } else {
                                        // Update the ACL after successful upload
                                        const aclParams = {
                                            Bucket: bucketName,
                                            Key: key,
                                            ACL: 'public-read' // Set the access control level to make the file public
                                        };

                                        s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                            if (aclErr) {
                                                console.error('Error updating ACL:', aclErr);
                                            } else {
                                                console.log('File uploaded successfully. Public URL:', data);
                                                const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                                const dataTest = {
                                                    imageLink: data.Location,
                                                    status: 'Success'
                                                };
                                                axios
                                                    .put(updateitems, dataTest).then((response) => {
                                                        console.log("Response is wow", response.data);
                                                    })
                                                    .catch((error) => {
                                                        console.log("Response is nah", error);
                                                    })
                                            }
                                        });
                                    }
                                });

                            }
                            else {
                                // console.log("check false", await page.locator('(//div[contains(@class, "p-3 text-color-error")])').textContent())
                                throw new Error("Code Not Sent");
                            }
                            totalRequest++;
                            await page.close();
                            await browser.close();
                        }
                        catch (error) {
                            const errorMessage = error.message || 'Unknown error';
                            totalRequestFailed++;
                            console.log("error alertssss", error)
                            console.log("checkingg", await page.screenshot({ path: 'screenshot.png' }))
                            await page.waitForTimeout(5000);
                            const key = `${user.jobObject.id}/${user.id}/image.png`;
                            filePath = './screenshot.png';
                            fileContent = fs.readFileSync(filePath);
                            // Set up the parameters for the S3 upload
                            const params = {
                                Bucket: bucketName,
                                Key: key,
                                Body: fileContent
                            };

                            // Upload the file to S3
                            s3.upload(params, (err, data) => {
                                if (err) {
                                    console.error('Error uploading file to S3:', err);
                                } else {
                                    // Update the ACL after successful upload
                                    const aclParams = {
                                        Bucket: bucketName,
                                        Key: key,
                                        ACL: 'public-read' // Set the access control level to make the file public
                                    };

                                    s3.putObjectAcl(aclParams, (aclErr, aclData) => {
                                        if (aclErr) {
                                            console.error('Error updating ACL:', aclErr);
                                        } else {
                                            console.log('File uploaded successfully. Public URL:', data);
                                            const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                                            const dataTest = {
                                                imageLink: data.Location,
                                                status: 'Failed',
                                                reason: errorMessage
                                            };
                                            axios
                                                .put(updateitems, dataTest).then((response) => {
                                                    console.log("Response is wow", response.data);
                                                })
                                                .catch((error) => {
                                                    console.log("Response is nah", error);
                                                })
                                        }
                                    });
                                }
                            });
                            await page.close();
                            await browser.close();


                        }
                    }
                    console.log("before wait")
                    await delay(60000 * maxRandomWait)
                    // await new Promise(resolve => setTimeout(resolve, 60000 * maxRandomWait));
                    console.log("after wait")
                }
                else {
                    const updateitems = `https://awtm8jmvwj.execute-api.eu-north-1.amazonaws.com/dev/otpTestsGeneratedMonetization/${user.id}`;
                    const dataTest = {
                        reason: 'Job is Expired',
                        status: 'Cancelled'
                    };
                    await axios
                        .put(updateitems, dataTest).then((response) => {

                        })
                        .catch((error) => {

                        })
                }
            }
        }
        else {
            console.log("job is stopped")
            // Cancel the job if it has already run twice
            job.cancel(true);
        }
    });
}

module.exports = runPlaywright;
