// example.js
const { test, expect } = require('@playwright/test');
const fs = require('fs-extra');
const csv = require('csv-parser');
const robot = require('robotjs');
const filePath = './users.csv';
const { chromium } = require('playwright-extra')
const pluginStealth = require("puppeteer-extra-plugin-stealth");
const puppeteer = require('puppeteer-extra');
const pathToExtension = require('path').join(__dirname, 'noCaptchaAi-chrome-v1.3');


// const CaptchaSolver = require('captcha-solver')

// const solver = new CaptchaSolver('browser')
// const codes = await solver.solve()

puppeteer.use(pluginStealth);

// Use RecaptchaPlugin



function getRandomInRange(min, max) {
    return Math.random() * (max - min) + min;
}

// Function to generate a random latitude
function getRandomLatitude() {
    return getRandomInRange(-90, 90);
}

// Function to generate a random longitude
function getRandomLongitude() {
    return getRandomInRange(-180, 180);
}

async function runPlaywright(paramArray) {
    randomEmail = Math.random().toString(36).substring(7);
    let totalRequest = 0;
    let totalRequestSent = 0;
    let totalRequestFailed = 0;
    // const browser = await chromium.launchPersistentContext(userDataDir, {
    //     headless: false,
    //     args: [
    //       `--disable-extensions-except=${pathToExtension}`,
    //       `--load-extension=${pathToExtension}`
    //     ]
    //   });
    // const browser = await chromium.launch({
    //     headless: false,
    //     // args: ['--load-extension=noCaptchaAi-chrome-v1.3'],
    //     // proxy: {
    //     //     server: 'http://geo.iproyal.com:12321',
    //     //     username: 'iSk3szVWheMj4F2K',
    //     //     password: 'mlvmJrDV6mK3d4fH',
    //     // },
    // });
    let browser
    let userDataDir
    // try {
    // const context = await browser.newContext();
    for (const user of paramArray.data) {
        try {
            const currentDate = new Date();
            const hours = currentDate.getUTCHours().toString().padStart(2, '0');
            const minutes = currentDate.getUTCMinutes().toString().padStart(2, '0');
            const seconds = currentDate.getUTCSeconds().toString().padStart(2, '0');

            const numericTime = `${hours}${minutes}${seconds}`;
            userDataDir = './userData' + numericTime;
            browser = await chromium.launchPersistentContext(userDataDir, {
                headless: false,
                devtools: true,
                // proxy: {
                //         server: 'http://geo.iproyal.com:12321',
                //         username: 'iSk3szVWheMj4F2K',
                //         password: 'mlvmJrDV6mK3d4fH',
                //     },
                args: [
                    `--disable-extensions-except=${pathToExtension}`,
                    `--load-extension=${pathToExtension}`,
                ]
            });
            // await context.setGeolocation({ latitude: getRandomLatitude(), longitude: getRandomLongitude() });
            const page = await browser.newPage();

            await page.goto('https://www.facebook.com/');
            // Replace this line: await page.pause();

            // await page.pause();
            await page.click('[data-testid="open-registration-form-button"]');
            await page.waitForSelector('[name="firstname"]');
            await page.type('[name="firstname"]', user.first_name);
            await page.type('[name="lastname"]', user.last_name);
            await page.type('[name="reg_email__"]', user.number);
            await page.type('[name="reg_passwd__"]', user.password);
            await page.selectOption('[name="birthday_day"]', user.birthday_day);
            await page.selectOption('[name="birthday_month"]', user.birthday_month);
            await page.selectOption('[name="birthday_year"]', user.birthday_year);
            await page.click(`input[name="sex"][value="${user.gender}"]`);
            await page.click('(//button[text()="Sign Up"])[1]');
            await page.waitForTimeout(15000);

            if (await page.isVisible('//h3[contains(text(), "Do you already have a Facebook account?")]')) {
                await page.click('//a[contains(text(), "No, ")][1]');
                await page.waitForTimeout(10000);

                if (await page.isVisible('.x193iq5w.xeuugli.x13faqbe.x1vvkbs.x1xmvt09.x1lliihq.x1s928wv.xhkezso.x1gmr53x.x1cpjm7i.x1fgarty.x1943h6x.x14z4hjw.x3x7a5m.xngnso2.x1qb5hxa.x1xlr1w8.xzsf02u.x1yc453h')) {
                    await page.click('//span[text()="Continue"]');
                    await page.waitForTimeout(6000);
                }
                await page.waitForTimeout(6000);
            } else if (await page.isVisible('//input[@name="contactpoint"]')) {
                await page.type('//input[@name="contactpoint"]', user.number);
                await page.click('//span[contains(text(), "Send Login Code")]');
                await page.waitForTimeout(6000);
            }
            else if (await page.isVisible('//span[contains(text(), "We need more information")]')) {
                await page.click('//span[text()="Continue"]');
                await page.waitForTimeout(7000);
            }
            else if (await page.isVisible('//*[contains(text(), "Help us")]')) {
                // await page.waitForTimeout(1000);
                // await page.reload()
                await page.waitForTimeout(15000);
                await page.click('//span[text()="Continue"]');
                await page.waitForTimeout(2000);
                if (await page.isVisible('//span[contains(text(), "Add a mobile number or email")]')) {
                    await page.type('//input[@name="contactpoint"]', user.number);
                    await page.click('//span[contains(text(), "Send Login Code")]')
                    await page.waitForTimeout(2000);
                }
                await page.waitForTimeout(2000);
            }
            // if (await page.isVisible('//span[contains(text(), "Enter an email address")]')) {
            //     await page.type('//input[@name="email"]', randomEmail + user.birthday_year + user.first_name + '@gmail.com');
            //     await page.click('//span[contains(text(), "Send Login Code")]')
            //     await page.waitForTimeout(2000);
            //     // totalRequestSent++;
            //     // console.log("Total request sent", totalRequestSent)
            // }

            if (await page.isVisible('//span[contains(text(), "Enter the code we sent")]') || await page.isVisible('(//h2[contains(text(), "Enter the confirmation code from the text message")])[2]')) {
                totalRequestSent++;
                console.log("Total request sent", totalRequestSent)
            }
            await page.waitForTimeout(5000);
            await page.close();
            totalRequest++;
            await browser.close();
            // Remove the user data directory
            try {
                await fs.remove(userDataDir);
                console.log('User data directory removed successfully.');
            } catch (error) {
                console.error('Error removing user data directory:', error.message);
            }
        }
        catch (error) {
            console.log("error alert", error)
            await browser.close();

            // Remove the user data directory
            try {
                await fs.remove(userDataDir);
                console.log('User data directory removed successfully.');
            } catch (error) {
                console.error('Error removing user data directory:', error.message);
            }
        }
    }
    return {
        response: {
            totalRequest: totalRequest,
            totalRequestSent: totalRequestSent,
            totalRequestFailed: totalRequestFailed
        }
    };
    // }
    // catch (error) {
    //     console.log("error alert", error)
    //     await browser.close();

    //     // Remove the user data directory
    //     try {
    //         await fs.rmdir(userDataDir, { recursive: true });
    //         console.log('User data directory removed successfully.');
    //     } catch (error) {
    //         console.error('Error removing user data directory:', error.message);
    //     }
    // }
}

module.exports = runPlaywright;
