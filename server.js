const express = require('express');
const bodyParser = require('body-parser');
const runPlaywrightGoogle = require('./googleAutomation');
const runPlaywrightFacebook = require('./facebookAutomation');
const runPlaywrightLoginServices = require('./loginServices');
const app = express();
const port = 8050;
const cors = require('cors');

// Use body-parser middleware to parse JSON in the request body
app.use(bodyParser.json());
app.use(cors());
app.get('/health-check', async (req, res) => {
  try {
    res.status(200).send("I Am UP :)");
  } catch (error) {
    console.error('Error executing Playwright code:', error);
    res.status(500).send('Internal Server Error');
  }
});
app.post('/run-playwright-login_Services', async (req, res) => {
  try {
    const paramArray = req.body;
    console.log("params", paramArray);

    // await runPlaywrightGoogle(paramArray.paramArray);

    res.status(200).send(await runPlaywrightLoginServices(paramArray.paramArray));
  } catch (error) {
    console.error('Error executing Playwright code:', error);
    res.status(500).send('Internal Server Error');
  }
});
app.post('/run-playwright-google', async (req, res) => {
  try {
    const paramArray = req.body;
    console.log("params", paramArray);

    // await runPlaywrightGoogle(paramArray.paramArray);

    res.status(200).send(await runPlaywrightGoogle(paramArray.paramArray));
  } catch (error) {
    console.error('Error executing Playwright code:', error);
    res.status(500).send('Internal Server Error');
  }
});
app.post('/run-playwright-facebook', async (req, res) => {
  try {
    const paramArray = req.body;
    console.log("params", paramArray);

    // await runPlaywrightFacebook(paramArray.paramArray);

    res.status(200).send(await runPlaywrightFacebook(paramArray.paramArray));
  } catch (error) {
    console.error('Error executing Playwright code:', error);
    res.status(500).send('Internal Server Error');
  }
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
