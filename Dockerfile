# Use an official Node.js runtime as the base image
# FROM node:14.17.6-alpine3.13
FROM node

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package.json ./

# Install app dependencies
RUN npm install --force

# Copy the rest of the app's source code to the working directory
COPY . .

# Build the app
# RUN npm run build

# Specify the command to run when the container starts
CMD ["npm", "start"]