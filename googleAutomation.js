// example.js
const { test, expect } = require('@playwright/test');
const fs = require('fs');
const csv = require('csv-parser');
const robot = require('robotjs');
const filePath = './users.csv';
const { chromium } = require('playwright-extra')
const pluginStealth = require("puppeteer-extra-plugin-stealth");
const puppeteer = require('puppeteer-extra');

puppeteer.use(pluginStealth);
function getRandomInRange(min, max) {
    return Math.random() * (max - min) + min;
}

// Function to generate a random latitude
function getRandomLatitude() {
    return getRandomInRange(-90, 90);
}

// Function to generate a random longitude
function getRandomLongitude() {
    return getRandomInRange(-180, 180);
}
// fs.createReadStream(filePath)
//     .pipe(csv())
//     .on('data', (row) => {
//         csvData.push(row);
//     })
//     .on('end', () => {
//         console.log(csvData);
//     })
//     .on('error', (error) => {
//         console.error('Error:', error.message);
//     });

async function runPlaywright(paramArray) {
    // const browser = await chromium.launch({
    //     headless: false, // Set to true for headless mode, false for non-headless mode
    // });
    // chromium.use(pluginStealth)
    console.log("baa", paramArray)
    let totalRequest = 0
    let totalRequestSent = 0
    let totalRequestFailed = 0
    for (const user of paramArray.data) {
        const randomEmail = Math.random().toString(36).substring(7);
        const randomPassword = Math.random().toString(36).substring(7);
        const browser = await chromium.launch(
            {
                headless: false,
                // proxy: {
                //     server: 'http://geo.iproyal.com:12321',
                //     username: 'iSk3szVWheMj4F2K',
                //     password: 'mlvmJrDV6mK3d4fH',
                // },
                // args: ['--proxy-server=geo.iproyal.com:12321:iSk3szVWheMj4F2K:mlvmJrDV6mK3d4fH'],
            }
        );

        const page = await browser.newPage();
        // const context = await browser.newContext();
        // await context.setGeolocation({ latitude: getRandomLatitude(), longitude: getRandomLongitude() });
        await page.goto('https://accounts.google.com/');
        // await page.waitForURL('https://accounts.google.com/')
        await page.pause();
        await page.locator('(//span[text()="Create account"])').click();
        await page.locator('(//span[text()="For my personal use"])').click();
        await page.locator('//input[@id="firstName"]').click();
        await page.locator('//input[@id="firstName"]').type(user.first_name);
        await page.locator('//input[@id="lastName"]').click(user.last_name);
        await page.locator('//input[@id="lastName"]').type(user.last_name);
        await page.locator('(//span[text()="Next"])').click();
        await page.locator('//select[@id="month"]').selectOption({ value: user.birthday_month });
        await page.locator('//input[@id="day"]').type(user.birthday_day);
        await page.locator('//input[@id="year"]').type(user.birthday_year);
        await page.locator('//select[@id="gender"]').selectOption({ value: user.gender });
        await page.locator('(//span[text()="Next"])[1]').click();
        await page.waitForTimeout(6000);
        if (await page.locator('//div[@id="selectioni3" and contains(@class, "d3GVvd jGAaxb")]').isVisible()) {
            await page.locator('//div[@id="selectioni3" and contains(@class, "d3GVvd jGAaxb")]').click()
            await page.waitForTimeout(2000);
            await page.locator('//input[@aria-label="Create a Gmail address"]').click();
            await page.locator('//input[@aria-label="Create a Gmail address"]').type(randomEmail + user.birthday_year + user.first_name);
        }
        else {
            await page.locator('//input[@aria-label="Username"]').click()
            await page.locator('//input[@aria-label="Username"]').type(randomEmail + user.birthday_year + user.first_name);
        }
        await page.locator('(//span[text()="Next"])').click();
        await page.waitForTimeout(2000);
        await page.locator('//input[@aria-label="Password"]').click();
        await page.locator('//input[@aria-label="Password"]').type(user.password + randomPassword);
        // setTimeout(() => {
        //     const screenWidth = robot.getScreenSize().width;
        //     const screenHeight = robot.getScreenSize().height;

        //     // Move the mouse cursor to a random point on the screen
        //     const randomX = Math.floor(Math.random() * screenWidth);
        //     const randomY = Math.floor(Math.random() * screenHeight);

        //     robot.moveMouseSmooth(randomX, randomY);
        //   }, 1000);
        await page.locator('//input[@aria-label="Confirm"]').click();
        await page.locator('//input[@aria-label="Confirm"]').type(user.password + randomPassword);
        await page.waitForTimeout(1000);
        await page.locator('(//span[text()="Next"])').click();
        // setTimeout(() => {
        //     const screenWidth = robot.getScreenSize().width;
        //     const screenHeight = robot.getScreenSize().height;

        //     // Move the mouse cursor to a random point on the screen
        //     const randomX = Math.floor(Math.random() * screenWidth);
        //     const randomY = Math.floor(Math.random() * screenHeight);

        //     robot.moveMouseSmooth(randomX, randomY);
        //   }, 2000);
        if (await page.locator('(//span[text()="Error"])').isVisible() || await page.locator('(//span[text()="Something went wrong"])').isVisible()) {
            await page.close();
            totalRequestFailed++;
        }
        else {
            await page.locator('//input[@id="phoneNumberId"]').type(user.number);
            // setTimeout(() => {
            //   const screenWidth = robot.getScreenSize().width;
            //   const screenHeight = robot.getScreenSize().height;

            //   // Move the mouse cursor to a random point on the screen
            //   const randomX = Math.floor(Math.random() * screenWidth);
            //   const randomY = Math.floor(Math.random() * screenHeight);

            //   robot.moveMouseSmooth(randomX, randomY);
            // }, 1000);
            await page.waitForTimeout(1000);
            await page.locator('(//span[text()="Next"])').click();
            await page.waitForTimeout(2000);
            if (await page.locator('(//div[text()="This phone number has been used too many times"])').isVisible()) {
                await page.close();
                totalRequestFailed++;
            }
            else {
                await page.close();
                totalRequestSent++;
            }

        }
        totalRequest++;
    }
    console.log("Total Requests", totalRequest, "Sent", totalRequestSent, "Failed", totalRequestFailed)
    return {
        response: {
            totalRequest: totalRequest,
            totalRequestSent: totalRequestSent,
            totalRequestFailed: totalRequestFailed
        }
    }
}

module.exports = runPlaywright;
